<?php

declare(strict_types=1);

namespace Seowork\Api\Account;

use Seowork\Component\Request\RequestInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AccountCreateDto
 * @package Seowork\Api\Account
 *
 * @property int $projectId
 * @property string $host
 * @property string $token
 */
class AccountCreateDto implements RequestInterface
{
    #[Assert\NotBlank]
    #[Assert\Type(type: 'integer')]
    private int $projectId;

    #[Assert\NotBlank]
    #[Assert\Type(type: 'string')]
    private string $host;

    #[Assert\NotBlank]
    #[Assert\Type(type: 'string')]
    private string $token;

    public function __construct(Request $request)
    {
        $properties = $request->request->all();
        foreach ($properties as $name => $value) {
            if (!property_exists($this, $name)) {
                throw new BadRequestException("Invalid request body");
            }
            $this->$name = $value;
        }
    }

    public function __get($property)
    {
        return $this->$property;
    }

    public function __set($property, $value): void
    {
        $this->$property = $value;
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
