<?php

declare(strict_types=1);

namespace Seowork\Api\Account;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api')]
class AccountController extends AbstractController
{
    #[Route('/account', methods: ['GET'])]
    public function indexAction(): JsonResponse
    {
        return new JsonResponse(['data' => 'test get']);
    }

    #[Route('/account', methods: ['POST'])]
    public function createAction(AccountCreateDto $dto, ManagerRegistry $doctrine): JsonResponse
    {
        $repository = new AccountRepository($doctrine);
        return new JsonResponse($repository->create($dto)->jsonSerialize());
    }
}
