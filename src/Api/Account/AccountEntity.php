<?php

declare(strict_types=1);

namespace Seowork\Api\Account;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AccountEntity
 * @package Seowork\Api\Account
 *
 * @property int $id
 * @property int $projectId
 * @property string $token
 * @property string $host
 * @property string $status
 *
 */
#[ORM\Entity(repositoryClass: AccountRepository::class)]
#[ORM\Table(name: 'account')]
class AccountEntity implements \JsonSerializable
{
    const STATUS_CREATED = 0;
    const STATUS_ACTIVE = 1;

    private static array $statuses = [
        self::STATUS_CREATED => 'CREATED',
        self::STATUS_ACTIVE => 'ACTIVE',
    ];

    #[ORM\Id, ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: Types::INTEGER)]
    private ?int $id = null;

    #[ORM\Column(type: Types::INTEGER)]
    private int $projectId;

    #[ORM\Column(type: Types::STRING)]
    private string $host;

    #[ORM\Column(type: Types::STRING)]
    private string $token;

    #[ORM\Column(type: Types::STRING)]
    private string $status;

    public function __construct(array $properties = [])
    {
        $this->status = self::$statuses[self::STATUS_CREATED];
        foreach ($properties as $name => $value) {
            if (property_exists($this, $name)) {
                $this->$name = $value;
            }
        }
    }

    public function __get($property)
    {
        return $this->$property;
    }

    public function __set($property, $value): void
    {
        $this->$property = $value;
    }

    public static function statuses($code = null): array|string
    {
        return $code ? self::$statuses[$code] : self::$statuses;
    }

    public function jsonSerialize(): array
    {
        $out = get_object_vars($this);
        unset($out["token"]);
        return $out;
    }
}
