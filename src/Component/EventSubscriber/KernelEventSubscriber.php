<?php

declare(strict_types=1);

namespace Seowork\Component\EventSubscriber;

use JsonException;
use Seowork\Component\Response\ErrorResponseFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use function is_array;
use const JSON_THROW_ON_ERROR;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
final class KernelEventSubscriber implements EventSubscriberInterface
{
    public function __construct(private ErrorResponseFactory $errorResponseFactory)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::EXCEPTION => ['onKernelException', -1],
        ];
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $request = $event->getRequest();

        if ('json' !== $request->getContentType() || '' === $request->getContent()) {
            return;
        }

        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $exception) {
            throw new BadRequestHttpException('Invalid JSON body was received', $exception);
        }

        $request->request->replace(is_array($data) ? $data : []);
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $event->setResponse(
            $this->errorResponseFactory->createResponseFromException($event->getThrowable())
        );
    }
}
