<?php

declare(strict_types=1);

namespace Seowork\Component\Request;

use function count;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
final class ArgumentValueResolver implements ArgumentValueResolverInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return is_a($argument->getType(), RequestInterface::class, true);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $params = $request->attributes->get('_route_params');

        // creating new instance of custom request DTO
        $class = $params['request_type'] ?? $argument->getType();

        if (null === $class) {
            throw new RuntimeException('Class not defined');
        }
        $dto = new $class($request);

        // throw bad request exception in case of invalid request data
        $errors = $this->validator->validate($dto);

        if (count($errors) > 0) {
            throw new RequestValidationError($errors);
        }

        yield $dto;
    }
}
