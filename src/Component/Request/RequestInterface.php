<?php

declare(strict_types=1);

namespace Seowork\Component\Request;

use Symfony\Component\HttpFoundation\Request;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
interface RequestInterface
{
    public function __construct(Request $request);
}
