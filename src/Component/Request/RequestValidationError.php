<?php

declare(strict_types=1);

namespace Seowork\Component\Request;

use Seowork\Component\Response\ErrorDetailsContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
final class RequestValidationError extends BadRequestHttpException implements ErrorDetailsContainerInterface
{
    private ConstraintViolationListInterface $errors;

    public function __construct(ConstraintViolationListInterface $errors)
    {
        parent::__construct('Invalid request body was given.');

        $this->errors = $errors;
    }

    public function getErrorDetails(): array
    {
        $details = [];

        foreach ($this->errors as $violation) {
            $details[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $details;
    }
}
