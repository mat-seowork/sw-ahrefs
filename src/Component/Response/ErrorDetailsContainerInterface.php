<?php

declare(strict_types=1);

namespace Seowork\Component\Response;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
interface ErrorDetailsContainerInterface
{
    public function getErrorDetails(): array;
}
