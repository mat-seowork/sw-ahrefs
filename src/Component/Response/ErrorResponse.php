<?php

declare(strict_types=1);

namespace Seowork\Component\Response;

use JsonSerializable;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
final class ErrorResponse implements JsonSerializable
{
    private string $error;

    private array $details;

    public function __construct(string $error, array $details = [])
    {
        $this->error = $error;
        $this->details = $details;
    }

    public function jsonSerialize(): array
    {
        return [
            'error' => $this->error,
            'details' => $this->details,
        ];
    }
}
