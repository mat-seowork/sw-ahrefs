<?php

declare(strict_types=1);

namespace Seowork\Component\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * @author Sergey Ageev (Cimus <s_ageev@mail.ru>)
 */
final class ErrorResponseFactory
{
    public function createResponseFromException(Throwable $exception): JsonResponse
    {
        return new JsonResponse(
            new ErrorResponse($exception->getMessage(), $this->extractDetails($exception)),
            $this->extractStatusCode($exception)
        );
    }

    private function extractStatusCode(Throwable $exception): int
    {
        return $exception instanceof HttpException ?
                $exception->getStatusCode() :
                Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    private function extractDetails(Throwable $exception): array
    {
        return $exception instanceof ErrorDetailsContainerInterface ?
                $exception->getErrorDetails() :
                [];
    }
}
